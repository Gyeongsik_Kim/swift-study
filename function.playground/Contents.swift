//: Playground - noun: a place where people can play

import Cocoa

/* Default Function Roll
    func [Function_Name]([Prameter_Name] : [Prameter_Type]) -> [Return_Type] {
    //DO SOMETHING
 
 }
 
 if function inner value name equal outer value name, using #
 example 
 var test
 func swap(#test....
 

 Default prameter value 
 if not send prameter, set value	
 func test(t : Int32 = 2...
*/
func swap(inout x : Int32, inout y : Int32) -> Void {
    let temp = x
    x = y
    y = temp
}

func average(X : Int32 ...) -> (Int32, Int32, Double){
     var max = Int32.min
     var min = Int32.max
     var total : Double = 0.0
     for temp in X {
     total += Double(temp)
        max = (max < temp) ? temp : max
        min = (min > temp) ? temp : min
     }
     return (min, max, total / Double(X.count))
}
    
func average(X : Int32 ...) -> Double {
    var total : Double = 0.0
    for temp in X{
        total += Double(temp)
    }
    return total / Double(X.count)
}

func add(X : Int) -> Int{
    return X +2
}
    
    
let x : Int32 = 0
let y : Int32 = 5
print("\(x), \(y)")
swap(&x, &y)
print("\(x), \(y)")
    
print("ADD TEST : 3 + 2 = \(add(3))")
print("AVERAGE TEST : 32, 23, 15 = \(ave(32, 23, 15)")

var temp = average(32, 23, 15)
print("MIN : \(temp.0)")
print("MAX : \(temp.1)")
print("AVERAGE : \(temp.2)")