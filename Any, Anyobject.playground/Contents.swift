//: Playground - noun: a place where people can play
//Type Any can save any type value
//Type AnyObject can save any type Object (like class)

class tt {
    let t = 53;
}

var test : Any = "GyungDal"
test = 213

//Type castring "as"

let tInt : Int32? = test as? Int32
var tes2 : AnyObject = tt()
print(test)
print(tes2)

print("String as Int \(tInt)")
print("Type String include Any \(tInt is Any)")