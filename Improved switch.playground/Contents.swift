//: Playground - noun: a place where people can play

import Cocoa

// swift switch can include range, string...

var str = "Hello, playground"

let test : UInt64 = 24
let test2 : String = "GyungDal"

switch test {
case 0 ... 30 :
    print("Low")
    break
case 31 ... 60 :
    print("Middle")
    break
case 60 ... 100 :
    print("High")
    break
default :
    print("Out of range")
    break
}

switch test2 {
case "GyungDal" :
    print("yeah")
    break
default :
    print("fail")
    break
}