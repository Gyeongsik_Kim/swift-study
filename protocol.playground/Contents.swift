//: Playground - noun: a place where people can play

protocol proto {
    var data : String? { get}
    var to : String {get}
    func send()
}

struct tcp : proto {
    var data : String?
    var to : String
    
    func send() {
        print("\(self.data) send to \(self.to)")
    }
}

func sendAnyThing(p : proto){
    p.send()
}

let t = tcp(data : "GyungDal", to : "9274aa@gmail.com")
sendAnyThing(t)