//: Playground - noun: a place where people can play

import Cocoa

class test{
    private var x : Double?
    private var y : Double?
    
    init(){
        x = Double(0.0)
        y = x
    }
    
    init(x : Double, y : Double){
        self.x = x
        self.y = y
    }
    /*
    convenience init(){
        super.init()
    }
    */
    func getX() -> Double?{
        return self.x
    }
    
    func getY() -> Double?{
        return self.y
    }
    
    deinit {
        print("Dead")
    }
}

var t = test(x : 4.65, y : 3.32)

print(t.getX())
