//: Playground - noun: a place where people can play

import Foundation

class FV304: NSObject {
    
    static let itself = FV304()
    
    var size: Int = 0
    var gun: String = ""
    var engin: String = ""
    var hp: Int = 0
    
    func Summary() -> (size: Int, gun: String, engin: String, hp: Int) {
        return(size, gun, engin, hp)
    }
}


FV304.itself.size = 100 // Previous 80
FV304.itself.gun = "4.5-inch Howizer"
FV304.itself.engin = "Unknown"
FV304.itself.hp = 280

let fv304_sum = FV304.itself.Summary()

