//
//  main.swift
//  zeroChan
//
//  Created by GyungDal on 10/21/16.
//  Copyright © 2016 GyungDal. All rights reserved.
//

import Foundation

let arg = CommandLine.arguments

let url = arg[1]

let testUrl = NSURL(string: url)
var html = NSString()
do {
    html = try NSString(contentsOfURL: testUrl, encoding: NSUTF8StringEncoding)
} catch{
    print(error)
}
print(html)
