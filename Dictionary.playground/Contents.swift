//: Playground - noun: a place where people can play
// Swift Dictionary look like Java HashMap

import Cocoa

/* How to make Dictionary 
    [DICTIONARY_NAME] = Dictionary<[KEY_TYPE], [VALUE_TYPE]>()
    [DICTIONARY_NAME] = [[KEY_TYPE], [VALUE_TYPE]]()
 
    Make empty Dictionary
    DICTIONARY_NAME] = [:]
*/
		
func printall(temp : Dictionary<String, String>) -> Bool {
    if(!temp.isEmpty){
        for key in temp.keys{
            print("Key : \(key), Value : \(temp[key])")
        }
        return true;
    }else{
        print("Empty Dictionary")
        return false;
    }
}
var test = ["GyungDal" : "admin", "guest" : "Hey"]

//print("Key : GyungDal, Value : \(test["GyungDal"])")
printall(test)


/* Update Return 
    if have not value, return nil
    but if have value, return old value 
 */

if test.updateValue("Change", forKey: "gust") == nil {
    print("Not found value")
}
    