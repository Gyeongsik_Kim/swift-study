//: Playground - noun: a place where people can play

import Cocoa

/* How to using Array 
   [VAR_NAME] : [[ARRAY_TYPE]] = [ARRAY_VALUE]
*/

let test : [String] = ["I", "want", "go", "to", "home"]
print("\(test[1])")

for i in 0 ... test.count-1{
    print("\(test[i])")
}