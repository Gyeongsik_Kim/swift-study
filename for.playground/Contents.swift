//: Playground - noun: a place where people can play

import Cocoa

/* 
    swift for in range : 
    1. for [VAR_NAME] in [START_NUM] ... [END_NUM]
    2. for [VAR_NAME] in [ARRAY]
*/

for i in 3 ... 6 {
    print("\(i)\n")
}

let array : [Int32] = [32,31,3,21]

for i in array {
    print("\(i)\n")
}
