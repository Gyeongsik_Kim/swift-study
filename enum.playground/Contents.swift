//: Playground - noun: a place where people can play

enum data : UInt8 {
    case Zero = 0
    case One
    case Two
    case Three
    case Four
    case Five
    case Six
    case Seven
    case Eight
    case Nine
    
    func get() -> String {
        switch self{
        case .Zero :
            	return "Zero"
        case .One :
            return "One"
        case .Two :
            return "Two"
        case .Three :
            return "Three"
        case .Four:
            return "Four"
        case .Five :
            return "Five"
        case .Six :
            return "Six"
        case .Seven :
            return "Seven"
        case .Eight :
            return "Eight"
        case .Nine :
            return "Nine"
        }
    }
}

let test = data.Five
print(test.rawValue)
print(test.get())